# pihole-installer

Fully interactive script to install and (re)setup a Pihole server, initially for openSUSE. See below for distros added. Feel free to contribute.  
A pihole server replaces the first DNS and blocks ad-services, so your local network can be ad-free, on all devices. See https://pihole.net
A pihole server can be run on any machine that's on 24/7 

Distros:
- openSUSE
- Debian
- Ubuntu and family
- Raspbian
- Red Hat
- Fedora
- Centos

Requirements:
- package manager ( zypper, yum, apt, more will follow )
- git
- zenity
- docker ( will be installed by the script if's not installed yet )
- Ports 53/tcp, 53/udp, 67/udp and the ports selected in the script must be opened in the firewall. ( might be automated in the near future )

Open a terminal window - no need to be root, you will be prompted by the script for your root/sudo password - and do
- git clone https://gitlab.com/knurpht/pihole-installer
- cd pihole-installer
- ./pihole-docker-install.sh

Right clicking on the script in a filemanager and 'execute' should work too, but you will not see the output of what the script is doing, or failing on.



Installs ( if not installed yet )
- docker

Enables and starts docker.service ( if not enabled and started yet)

Fires up pihole docker container with user entered values

Using the script agains makes it clean up the current container and image, so you can reconfigure the Pihole server if you want.


Have fun !!
