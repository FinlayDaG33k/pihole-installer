#
# spec file for package inxi
#
# Copyright (c) 2018 SUSE Gertjan Lettink <knurpht@opensuse.org
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the GPL-3.0 License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via https://gitlab.com/knurpht/
# 

%define _version 0.7-1

Name:           pihole-docker-install.sh
Version:        0.7
Release:        0
Summary:        A Pihole server setup and config tool
License:        GPL-3.0
Group:          System/DNS
URL:            https://gitlab.com/knurpht/pihole-installer
Source:         https://gitlab.com/knurpht/pihole-installer/pihole-docker-install.sh
Requires:       zenity
Requires:       docker
%if 0%{?suse_version} >= 1120
BuildArch:      noarch
%else
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
%endif

%description
Fully interactive script to install and (re)setup a Pihole server, initially for openSUSE. See below for distros added. Feel free to contribute.
A pihole server replaces the first DNS and blocks ad-services, so your local network can be ad-free, on all devices. See https://pihole.net
A pihole server can be run on any machine that's on 24/7

%prep
%setup -q -n %{name}-%{_version}
sed -i '/^#!/s/env \(.*\)$/\1/' %{name}

%build
# Nothing to build.

%install
install -Dpm 0755 %{name} %{buildroot}%{_bindir}/%{name}
install -Dpm 0644 %{name}.1 %{buildroot}%{_mandir}/man1/%{name}.1

%files
%if 0%{?suse_version} >= 1500
# %license LICENSE.txt
%else
%defattr(-,root,root)
# %doc LICENSE.txt
%endif
# %doc inxi.changelog README.txt
%{_bindir}/%{name}
# %{_mandir}/man1/%{name}.1%{?ext_man}

%changelog
